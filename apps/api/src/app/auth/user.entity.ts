import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Task } from '../tasks/task.entity';

@Entity()
export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    unique: true
  })
  public username: string;

  @Column({
    unique: true
  })
  public password: string;

  @Column({
    nullable: true
  })
  public salt: string;

  @OneToMany(() => Task, task => task.user, { eager: true })
  public tasks: Task[];

  async validationPassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
