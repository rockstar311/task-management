import { Injectable, NotFoundException } from '@nestjs/common';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { User } from '../auth/user.entity';

@Injectable()
export class TasksService {

  constructor(private taskRepository: TaskRepository) {
  }

  private static throwErrorTaskNotFound(id: number): void {
    throw new NotFoundException(`Task with ID "${id}" not found`);
  }

  public async updateStatusTask(
    id: number,
    status: TaskStatus,
    user: User): Promise<Task> {
    const task = await this.getTaskById(id, user);
    task.status = status;
    return task.save();
  }

  public getTasks(filterDto: GetTasksFilterDto, user: User): Promise<Task[]> {
    return this.taskRepository.getTasks(filterDto, user);
  }

  public async getTaskById(id: number, { id: userId }: User): Promise<Task> {
    const found = await this.taskRepository.findOne({ id, userId });

    if (!found) TasksService.throwErrorTaskNotFound(id);

    return found;
  }

  public createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    return this.taskRepository.createTask(createTaskDto, user);
  }


  public async deleteTask(id: number): Promise<void> {
    const { affected } = await this.taskRepository.delete(id);

    if (!affected) TasksService.throwErrorTaskNotFound(id);
  }
}
