import { BadRequestException, PipeTransform } from '@nestjs/common';
import { ArgumentMetadata } from '@nestjs/common/interfaces/features/pipe-transform.interface';
import { TaskStatus } from '../task-status.enum';

export class StatusValidationPipe implements PipeTransform {

  private readonly allAllowedStatus = [
    TaskStatus.Open,
    TaskStatus.InProgress,
    TaskStatus.Done
  ];

  transform(value: any, metaData: ArgumentMetadata): TaskStatus {
    value = value.toUpperCase();

    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`${value} is an invalid ${metaData.data}`);
    }

    return value;
  }

  private isStatusValid(status: any): boolean {
    return this.allAllowedStatus.includes(status);
  }
}
