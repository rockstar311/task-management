import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Task } from '../app/tasks/task.entity';
import { User } from '../app/auth/user.entity';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'taskmanagement',
  entities: [
    Task,
    User
  ],
  synchronize: true
};
