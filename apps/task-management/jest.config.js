module.exports = {
  name: 'task-management',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/task-management',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
